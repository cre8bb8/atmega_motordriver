#include <stdbool.h>

#include <avr/io.h>
#include <avr/sleep.h>

#include "pwm.h"
#include "TWI_Slave.h"


// TWI commands
#define MOTOR_LEFT 0
#define MOTOR_RIGHT 1
#define FORWARD 0
#define BACKWARD 1

int main(void){
    uint8_t TWIMessageSize = 3;
    uint8_t TWIMessageBuffer[TWIMessageSize];
    uint8_t TWISlaveAddress = 0x01;
    uint8_t motorSpeed[2];
    bool motorDir[2];

    setupPwm();

    TWI_Slave_Initialise((TWISlaveAddress<<TWI_ADR_BITS) | (1<<TWI_GEN_BIT));
    TWI_Start_Transceiver();

    while(1){
        // enter sleep if possible
        if( ! TWI_statusReg.RxDataInBuf ){
            if(TWI_Transceiver_Busy()){
                MCUCR = (1<<SE)|(0<<SM2)|(0<<SM1)|(0<<SM0); // Enable sleep with idle mode
                }
            else{
                MCUCR = (1<<SE)|(0<<SM2)|(1<<SM1)|(0<<SM0); // Enable sleep with power-down mode
            }
            sleep_cpu();
        }

        if(!TWI_Transceiver_Busy()){
            if(TWI_statusReg.RxDataInBuf){
                TWI_Get_Data_From_Transceiver(TWIMessageBuffer, TWIMessageSize);

                motorSpeed[0] = TWIMessageBuffer[1];
                motorSpeed[1] = TWIMessageBuffer[2];

                motorDir[0] = (TWIMessageBuffer[0] >> LEFT) & 1
                motorDir[1] = (TWIMessageBuffer[0] >> RIGHT) & 1

                setMotorLeft(motorDir[0],motorSpeed[0]);
                setMotorRight(motorDir[1],motorSpeed[1]);
            }
        }
    }

    return 0;
}
