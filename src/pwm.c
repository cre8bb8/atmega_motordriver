#include "pwm.h"

#include <avr/io.h>

void setupPwm(void){
    // do non-inverting PWM (set Pin to 0 when matching)
    TCCR1A = (1<<COM1A1) | (1 << COM1B1);
    // enable phase/frequency correct PWM with resolution of ICR1
    TCCR1B = (1<<WGM13);
    
    // use 1/8th prescaler
    TCCR1B |= (1 << CS11);
    
    // set 0xFF as TOP (resolution of 8 bit)
    ICR1H = 0;
    ICR1L = 0xFF;

    // set ports as outputs
    DDRB |= (1<<PB1) | (1<<PB2);
    DDRC |= (1<<PC0) | (1<<PC1);
    
    // reset counter
    TCNT1H = 0;
    TCNT1L = 0;

}

void setMotorLeft(bool direction, uint8_t speed){
    if(direction){
        // invert PWM duty cycle
        speed = 0xff-speed;
        // reverse direction (OC1A low => Motor turns)
        PORTC |= (1<<PC0);
    }
    else{
        // normal direction (OC1A high => motor turns)
        PORTC &= ~(1<<PC0);
    }
    // set PWM duty cycle
    OCR1A = speed;
}

void setMotorRight(bool direction, uint8_t speed){
    if(direction){
        // invert PWM duty cycle
        speed = 0xff-speed;
        // reverse direction (OC1A low => Motor turns)
        PORTC |= (1<<PC1);
    }
    else{
        // normal direction (OC1A high => motor turns)
        PORTC &= ~(1<<PC1);
    }
    // set PWM duty cycle
    OCR1B = speed;
}
