#ifndef ATMEGA_MOTORCONTROLLER_PWM_H
#define ATMEGA_MOTORCONTROLLER_PWM_H

#include <stdint.h>
#include <stdbool.h>

// initial setting of registers
void setupPwm(void);

void setMotorLeft(bool direction, uint8_t speed);

void setMotorRight(bool direction, uint8_t speed);

#endif // include guard
