## motordriver project Makefile

# include board specific configuration
include config.mk

# tie boardconfig together
BOARDCONFIG = -DF_CPU=$(F_CPU)

# names of output files
KERNELPREFIX = kernel
KERNELF = $(KERNELPREFIX).elf
KERNHEX = $(KERNELPREFIX).hex
KERNMAP = $(KERNELPREFIX).map
KERNASM = $(KERNELPREFIX).asm

.PHONY: all
all: $(KERNHEX) $(KERNASM)

# configuration
TRIPLET = avr

# configure assembler
AS = $(TRIPLET)-as
ASOPT = --mmcu=$(MCU) $(BOARDCONFIG)

CC = $(TRIPLET)-gcc
CCOPT = -MD -MP -O2 -std=c99 -mmcu=$(MCU) $(BOARDCONFIG)

# configure linker
LDOPT = -Wl,--error-unresolved-symbols,-Map,$(KERNMAP) -g -mmcu=$(MCU)

# configure objcopy
OBJCOPY = $(TRIPLET)-objcopy
OBJCOPYOPT = -j .text -j .data -O ihex

# configure objdump
OBJDUMP = $(TRIPLET)-objdump

# lists of files
ASRC = $(wildcard src/*.s)
AOBJ = $(ASRC:.s=.o)

CSRC = $(wildcard src/*.c)
COBJ = $(CSRC:.c=.o)

OBJS = $(AOBJ) $(COBJ)


# include depend files
-include $(COBJ:.o=.d)

# compile assembler source files
%.o : %.s
	@echo -n "Assembling '$<'... "
	@$(AS) $(ASOPT) -o $@ $<
	@echo "done."

# compile C source files
%.o : %.c
	@echo -n "Compiling '$<'... "
	@$(CC) $(CCOPT) -c -o $@ $<
	@echo "done."

#############################
# targets 
#############################

# produce final kernel image
$(KERNELF) : $(OBJS)
	@echo -n "Linking kernel... "
	@$(CC) $(LDOPT) -o $(KERNELF) $(OBJS)
	@echo "done."

$(KERNHEX) : $(KERNELF)
	@echo -n "Generating hexdump... "
	@$(OBJCOPY) $(OBJCOPYOPT) $(KERNELF) $(KERNHEX)
	@echo "done."

# produce assembler dump of kernel image
$(KERNASM) : $(KERNELF)
	@echo -n "Dumping Assembler of kernel... "
	@$(OBJDUMP) -D $(KERNELF) > $(KERNASM)
	@echo "done."

# remove output files
clean:
	@rm -f $(OBJS) $(COBJ:.o=.d) $(KERNELF) $(KERNASM) $(KERNMAP) $(KERNHEX)

