# BB8's motor controller 

## What's it about

It's supposed to be burned on an Atmega Microcontroller,
which operates the motor controller of BB8.

The motor controller board consists of two
[H Bridges](https://en.wikipedia.org/wiki/H_bridge),
driving the motors.
The Atmega controls the direction and speed,
and is operated by sending motor commands (see below)
via I2C bus.


## motor commands

a motor command is three (3) bytes long:

* a bitfield for the directions  

|           | left off | left on |
|----------|----------|--------|
| **right off** | 0x00     | 0x01 |
| **right on**  | 0x02     | 0x03 |

* a uint8 for the speed of the left motor
* a uint8 for the speed of the right motor


## installation

* copy `config.mk.default` to `config.mk`
* edit it according to your board layout
* compile with `make`
* use the programmer of your trust to burn the program
  to the AVR (e.g. avrDUDE)

## Links

* [Project Wiki](https://bitbucket.org/cre8bb8/doc/wiki/Home)

